﻿using UnityEngine;
using System.Collections;

public class BGMManager : MonoBehaviour {
    private GameManager mGmr;
    private AudioSource bgm;

    public AudioClip FactoryBGM;
    public AudioClip CourtyardBGM;
    float pauseTime;

	// Use this for initialization
	void Start () {
        mGmr = GetComponent<GameManager>();
        bgm = GetComponent<AudioSource>();
        bgm.clip = FactoryBGM;
		bgm.loop = true;
        bgm.Play();
    }

    public void SwitchBGM()
    {
        pauseTime = bgm.time;
        bgm.Stop();

        if (mGmr.currentLayer == mGmr.Courtyard)
        {
            bgm.clip = CourtyardBGM;
            
        }
        else if (mGmr.currentLayer == mGmr.Factory)
        {
            bgm.clip = FactoryBGM;  
        }
        bgm.timeSamples = (int)pauseTime;
        bgm.Play();
    }
}
