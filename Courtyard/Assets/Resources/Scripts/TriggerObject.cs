﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TriggerObject : MonoBehaviour {

    public enum TriggerType {
        None,
        Button,
        Lever,
        Valve,
        ConveyerBelt,
        BoxLid
    }
    public TriggerType TriggerIdentifier = TriggerType.None;
    
    public GameObject[] otherObjects;

    public int initialTriggerCount = 1;
    private int triggerCount;
    public bool clickable = true;

    private List<TriggerHelper> childScripts;

	// Use this for initialization
    void Awake()
    {
        childScripts = new List<TriggerHelper>();
    }


    void Start() {
        triggerCount = initialTriggerCount;
        for (int i = 0; i < otherObjects.Length; i++)
        {
            otherObjects[i].SetActive(false);
        }
        

    }
	
	// Update is called once per frame
	void Update () {
	    
	}

    public virtual void Clicked()
    {
        if (clickable)
        {
            GetComponentInParent<MachineSFX>().PlaySFX(TriggerIdentifier);
            if (triggerCount > 0)
            {
                triggerCount--;
            }
            
            foreach (TriggerHelper iter in childScripts)
            {
                iter.Clicked();
            }
            
            if (triggerCount <= 0)
            {
                ActivateOther();
                triggerCount = initialTriggerCount;
            }
        }

    }

    public void ActivateOther()
    {
        //Debug.Log("Activating " + otherObject);
        for (int i = 0; i < otherObjects.Length; i++)
        {
            if (otherObjects[i])
            otherObjects[i].SetActive(true);
            TransformableObject t = otherObjects[i].GetComponent<TransformableObject>();
            if (t)
            {
                t.EnableTransformation();
            }
        }
        clickable = false;
    }

    public void RegisterScript(TriggerHelper tScript)
    {
        childScripts.Add(tScript);
    }

    public void EnableClick()
    {
        clickable = true;
    }

    public void DisableClick()
    {
        clickable = false;
    }
}
