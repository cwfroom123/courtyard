﻿using UnityEngine;
using System.Collections;

public class LanternEventHolder : MonoBehaviour {

    public GameObject SceneFadeController;
	private AudioSource sfx;
	public AudioClip FiringAudio;
	public AudioClip TalkingAudio;
	private bool talking = false;
	float pauseTime = 0;

	void Start(){
		sfx = GetComponent<AudioSource> ();
	}


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<ItemType>() != null)
        {
            if (other.gameObject.GetComponent<ItemType>().GetObjectTag() == "Lantern" &&
                other.gameObject.GetComponent<ItemType>().onHand == false)
            {
                Debug.Log("Reached holder");

                // Hang lantern
                other.gameObject.GetComponent<Collider>().enabled = false;
                other.gameObject.GetComponent<Rigidbody>().useGravity = false;
                other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                other.gameObject.GetComponent<Light>().enabled = true;

				// Check point reached, reset tiredness
                SceneFadeController.GetComponent<FadeController>().Reset();
				FindObjectOfType<EndGameManager> ().ActivateLantern ();

				//play firing sound
				sfx.clip = FiringAudio;
				sfx.Play();
				StartCoroutine (playSound ());
            }
        }
    }

	IEnumerator playSound()
	{
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
		talking = true;
		sfx.clip = TalkingAudio;
		sfx.loop = true;
		sfx.Play();
	}

	void OnEnable(){
		if (talking) {
			sfx.timeSamples = (int)pauseTime;
			sfx.Play();	
		}

	}

	void OnDisable(){
		if (talking) {
			pauseTime = sfx.time;
			sfx.Pause();
		}
	}
}
