﻿using UnityEngine;
using System.Collections;

public class MovementTrigger : MonoBehaviour {


    public GameObject MovingTarget;
    Transform movingPosition;

    bool lerpTo = false;

    float timeTakenDuringLerp = 5f;
    float timeStartedLerping;

    public Vector3 movingDistance; 


    // Use this for initialization
    void Start () {
        movingPosition = MovingTarget.transform;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter (Collider hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            lerpTo = true;
           
            ObjectMove();

            timeStartedLerping = Time.time;
        }
    }
    void ObjectMove()
    {
        if (lerpTo == true)
        {
            float timeSinceStarted = Time.time - timeStartedLerping;
            float percentageComplete = timeSinceStarted / timeTakenDuringLerp;
            // Object start move
            movingPosition.position = Vector3.Lerp(movingPosition.position, movingPosition.position + movingPosition.right * movingDistance.x, percentageComplete);
            if (movingPosition.position == movingPosition.position + movingPosition.right*movingDistance.x)
            {
                lerpTo = false;            }
        }
    }
}
