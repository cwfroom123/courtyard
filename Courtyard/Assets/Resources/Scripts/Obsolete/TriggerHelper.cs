﻿using UnityEngine;
using System.Collections;

public class TriggerHelper : MonoBehaviour {
    protected TriggerObject mOwner;

    public virtual void Start() {
        mOwner = GetComponent<TriggerObject>();
        mOwner.RegisterScript(this);
    }
    public virtual void Update() { }

    public virtual void Clicked() { }
}
