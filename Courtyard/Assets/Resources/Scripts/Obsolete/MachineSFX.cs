﻿using UnityEngine;
using System.Collections;

public class MachineSFX : MonoBehaviour {
    private AudioSource sfx;
    public AudioClip PressButtonSound;
    public AudioClip ConveyerBeltSound;
    public AudioClip PushLeverSound;
    public AudioClip TurnValveSound;


	// Use this for initialization
	void Start () {
        sfx = GetComponent<AudioSource>();
        
	}

    public void PlaySFX(TriggerObject.TriggerType trigger)
    {
        switch (trigger) {
            case TriggerObject.TriggerType.Button:
                sfx.clip = PressButtonSound;
                break;
            case TriggerObject.TriggerType.Valve:
                sfx.clip = TurnValveSound;
                break;
            case TriggerObject.TriggerType.Lever:
                sfx.clip = sfx.clip = PushLeverSound;
                break;
            case TriggerObject.TriggerType.ConveyerBelt:
                sfx.clip = ConveyerBeltSound;
                break;
        }

        sfx.Play();
    }

    
}
