﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class FadeController : MonoBehaviour {

    public Animator screenFade;
    public GameObject eyeMask;

    //float startTime;
    //float endTime;

    public bool eyeClosed;

    //public float gameDuration = 60.0f;
    public float fatigueDuration = 30.0f;
    public float blinkInterval = 0.3f;

    bool startBlink;
    bool stopBlink;
    int counter;

	void Start () {
        //startTime = Time.time;
        startBlink = false;
        stopBlink = false;
        eyeClosed = false;
        counter = 0;
	}
	
	void Update () {
        //endTime = Time.time;

        Color maskColor = eyeMask.GetComponent<Image>().color;

        if (maskColor.a < 1.0f) {
            maskColor.a += (1 / fatigueDuration) * Time.deltaTime;
            maskColor.a = Mathf.Clamp(maskColor.a, 0.0f, 1.0f);
            eyeMask.GetComponent<Image>().color = maskColor;
        }

        if (maskColor.a == 1.0f && !startBlink) {
            startBlink = true;
            StartCoroutine("EyeBlinking");
        }

        if (stopBlink) // *endTime - startTime >= gameDuration
        { 
            stopBlink = false;
            GameOver();
        }
	}

    void GameOver() {
        Debug.Log("Game over!");
        SceneManager.LoadScene("MainMenu");
    }

    IEnumerator EyeBlinking() {
        counter++;
        screenFade.SetTrigger("FadeTrigger");

        eyeClosed = true;

        yield return new WaitForSeconds(2.0f);

        eyeClosed = false;

        screenFade.SetTrigger("FadeTrigger");

        yield return new WaitForSeconds(3.0f);

        if (counter < 3)
        {
            StartCoroutine("EyeBlinking");
        }
        else {
            screenFade.SetTrigger("FadeTrigger");
            yield return new WaitForSeconds(3.0f);
            stopBlink = true;
        }
    }

    // For check point reset
    public void Reset() {
        if (eyeClosed) {
            screenFade.SetTrigger("FadeTrigger");
        }

        StopCoroutine("EyeBlinking");

        counter = 0;
        eyeClosed = false;
        stopBlink = false;
        startBlink = false;
        Color maskColor = eyeMask.GetComponent<Image>().color;
        maskColor.a = 0.0f;
        eyeMask.GetComponent<Image>().color = maskColor;
    }
}
