﻿using UnityEngine;
using System.Collections;

public class LeverMachine : MonoBehaviour {

    private int leverPushed = 0;
    public GameObject box;

	// Use this for initialization
	void Start () {
        box.SetActive(false);
	}
	
    public void PushLever()
    {
        leverPushed++;
        if (leverPushed >= 5)
        {
            box.SetActive(true);
        }
    }
}
