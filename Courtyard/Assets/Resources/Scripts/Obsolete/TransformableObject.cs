﻿using UnityEngine;
using System.Collections;

public class TransformableObject : MonoBehaviour
{

    [HideInInspector]
    public bool Transformable;
    public GameObject otherObject;

    void Awake()
    {
        Transformable = false;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    
    public void EnableTransformation()
    {
        Transformable = true;
    }



}
