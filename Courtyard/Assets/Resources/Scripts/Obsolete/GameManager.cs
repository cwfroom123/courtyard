﻿using UnityEngine;
using System.Collections;


public class GameManager : MonoBehaviour {

    public Camera FPSCamera;
    public LayerMask Courtyard;
    public LayerMask Factory;
    [HideInInspector] public LayerMask currentLayer;

    public KeyCode SwitchKey = KeyCode.E;
    public static string LayerString = "Factory";
	public UnityStandardAssets.Characters.FirstPerson.FirstPersonController FPSControl;

	void Start ()
    {
        currentLayer = Factory;
        //Show couryard hide factory
		FPSControl.enabledControl = false;
        FPSCamera.cullingMask &= Factory.value;
    }

	void Update ()
    {
	    if (Input.GetKeyDown(SwitchKey))
        {
            SwitchLayer();
        }
	}

    void SwitchLayer() {

        if (currentLayer == Courtyard)
        {
			FPSControl.enabledControl = false;
            currentLayer = Factory;
            LayerString = "Factory";
            FPSCamera.cullingMask = -1 & Factory.value;
            Debug.Log(LayerString);
        }
        else
        {
			FPSControl.enabledControl = true;
            currentLayer = Courtyard;
            LayerString = "Courtyard";
            FPSCamera.cullingMask = -1 & Courtyard.value;
            Debug.Log(LayerString);
        }
        GetComponent<BGMManager>().SwitchBGM();
    }
}
