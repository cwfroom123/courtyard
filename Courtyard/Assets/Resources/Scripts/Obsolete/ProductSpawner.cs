﻿using UnityEngine;
using System.Collections;

public class ProductSpawner : MonoBehaviour {
    //Spawn one product when activated
    public GameObject screwDriver;

    void Awake()
    {
        this.gameObject.SetActive(false);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnEnable()
    {
        Instantiate(Resources.Load("Prefabs/Product"),transform.position,transform.rotation);
        this.gameObject.SetActive(false);
        GetComponentInParent<MachineSFX>().PlaySFX(TriggerObject.TriggerType.ConveyerBelt);
    }
}
