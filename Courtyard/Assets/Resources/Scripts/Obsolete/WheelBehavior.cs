﻿using UnityEngine;
using System.Collections;
using System;

public class WheelBehavior : TriggerHelper {
    public float rotationStep = 5.0f;

    public float eachRot = 90.0f;
    

    public override void Start()
    {
        base.Start();
    }

    public override void Update(){}

    public override void Clicked()
    {
        //turn the wheel

        mOwner.DisableClick();

        StartCoroutine(Rotate(eachRot));
    }


    IEnumerator Rotate(float remaining)
    {
        float r = remaining;
        r -= rotationStep;
        transform.Rotate(new Vector3(0.0f, 0.0f, rotationStep), Space.World);
        
        yield return new WaitForSeconds(Time.deltaTime);
        if (r > 0)
        {
            StartCoroutine(Rotate(r));
        }
        else
        {
            mOwner.EnableClick();
        }
        
    }
    
}
