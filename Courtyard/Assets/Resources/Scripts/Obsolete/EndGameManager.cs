﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndGameManager : MonoBehaviour {

    // Only for prototype
	public static int lanternRemaining = 4;
    public static bool TableFlag = false;
    public static bool LanternFlag = false;

    public GameObject doors;

	public void ActivateTable(){
		TableFlag = true;
		IfGameEnd ();
	}

	public void ActivateLantern(){
		lanternRemaining--;
		if (lanternRemaining == 0) {
			LanternFlag = true;
		}
		IfGameEnd ();
	}

	public void IfGameEnd(){
		if (TableFlag && LanternFlag) {
			doors.SetActive(false);
			TableFlag = false;
			LanternFlag = false;
		}
	}


    void OnTriggerEnter(Collider other) {
        SceneManager.LoadScene("MainMenu");
    }
}
