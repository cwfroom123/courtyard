﻿using UnityEngine;
using System.Collections;

public class LevelBehavior : TriggerHelper {
    LeverMachine machine;

    public float rotationStep = 3.0f;

    public float eachRot = 40.0f;

    // Use this for initialization
    public override void Start () {
        base.Start();
        machine = GetComponentInParent<LeverMachine>();
	}

    public override void Clicked()
    {
        //turn the lever
        machine.PushLever();
        StartCoroutine(Rotate(eachRot));
    }

    IEnumerator Rotate(float remaining)
    {
        float r = remaining;
        r -= rotationStep;
        transform.Rotate(new Vector3(rotationStep, 0.0f,0.0f ), Space.World);

        yield return new WaitForSeconds(Time.deltaTime);
        if (r > 0)
        {
            StartCoroutine(Rotate(r));
        }

    }

}
