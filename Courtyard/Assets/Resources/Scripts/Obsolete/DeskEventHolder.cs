﻿using UnityEngine;
using System.Collections;

public class DeskEventHolder : MonoBehaviour {

   
	public GameObject TableWFood;

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.GetComponent<ItemType>() != null) {
            if (other.gameObject.GetComponent<ItemType>().GetObjectTag() == "Mop" &&
                other.gameObject.GetComponent<ItemType>().onHand == false) {
                Debug.Log("Reached holder");

                // Change to table with food
                other.gameObject.SetActive(false);
                transform.parent.SetAsLastSibling();
                transform.parent.gameObject.SetActive(false);
				TableWFood.SetActive (true);

              

				
            }
        }
    }
}
