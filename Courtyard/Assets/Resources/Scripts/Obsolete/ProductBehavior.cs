﻿using UnityEngine;
using System.Collections;

public class ProductBehavior : MonoBehaviour {
    Vector3 targetPos;

	// Use this for initialization
	void Start () {
        //length of belt
        targetPos = transform.position + new Vector3(10, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.x < targetPos.x)
        {
            transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * 2);
            
        }
        
    }
}
