﻿using UnityEngine;
using System.Collections;

public class LadderClimb : MonoBehaviour {
    public bool m_climbEnabled;
    public float m_heightFactor = 3.0f;

	// Use this for initialization
	void Start () {
        m_climbEnabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (m_climbEnabled && Input.GetKey("w"))
        {
            transform.position += Vector3.up / m_heightFactor;
        }	
	}

    void OnTriggerEnter(Collider collider)
    {

        if (collider.gameObject.tag == "Ladder")
        {
            m_climbEnabled = true;
        }
    }

    void OnTriggerExit(Collider collider)
    {

        if (collider.gameObject.tag == "Ladder")
        {
            m_climbEnabled = false;
        }
    }
}
