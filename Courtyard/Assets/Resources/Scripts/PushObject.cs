﻿using UnityEngine;
using System.Collections;

public class PushObject : MonoBehaviour {

    public float pushStrength = 6.0f;

    public GameObject firstPersonController;

    void Start ()
    {


        



    }

    void OnControllerColliderHit(ControllerColliderHit hit) {
        if (hit.gameObject.tag == "Movable") {

            Rigidbody rBody = hit.collider.attachedRigidbody;
           
          
            
            if (rBody == null) {
                return;
            }

            if (hit.moveDirection.y < -0.3) {
                return;
            }

            Vector3 pushDir;

            if (Mathf.Abs(hit.moveDirection.z) > Mathf.Abs(hit.moveDirection.x))
            {
                pushDir = new Vector3(0.0f, 0.0f, hit.moveDirection.z);
            }
            else
            {
                pushDir = new Vector3(hit.moveDirection.x, 0.0f, 0.0f);
            }
            rBody.velocity = pushDir * pushStrength / rBody.mass;
        }
    }
}
